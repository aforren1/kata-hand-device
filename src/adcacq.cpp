/** 
 *  @file    adcacq.cpp
 *  @author  Alexander Forrence, Jacob Carducci
 *  @date    06/15/2017
 *  @version 0.2
 *  
 *  @brief ADC channel DAQ function definitions.
 *
 *  @section DESCRIPTION
 *  
 * All things dealing with ADC ADQ, including helpers.
 *
 **/

#include <ADC.h>
#include <array>
#include <stdint.h>
#include "adcacq.h"
#include "Arduino.h"
#include "constants.h"
#include "hidmisc.h"

byte desiredRes = 16;
byte desiredAvg = 16;

std::array<uint, channels> recent_values; // temporary storage for values read off ADCs

ADC *adc = new ADC();

int diff;
byte buffer_adc[64];

// timers
elapsedMillis timestamp;
elapsedMicros timer_begin_adc;
elapsedMicros timer_between_adc;

unsigned long read_start = 0;
unsigned long current_time = 0;
unsigned long previous_time = 0;

unsigned int between_adc_period = 40; // period between starts of individual readings within an adc period

// counters
unsigned int ii;
unsigned int jj;

/**
*   @brief  Initializes the Teensy's ADC unit, including resolution, averaging, sampling speed, conversion speed, and reference voltage
*
*   @return void
**/
void setupADC(){    
    adc->setResolution(desiredRes, ADC_0);
    adc->setResolution(desiredRes, ADC_1);

    adc->setAveraging(desiredAvg, ADC_0);
    adc->setAveraging(desiredAvg, ADC_1);

    adc->setSamplingSpeed(ADC_SAMPLING_SPEED::HIGH_SPEED, ADC_0);
    adc->setConversionSpeed(ADC_CONVERSION_SPEED::HIGH_SPEED,ADC_0);

    adc->setSamplingSpeed(ADC_SAMPLING_SPEED::HIGH_SPEED,ADC_1);
    adc->setConversionSpeed(ADC_CONVERSION_SPEED::HIGH_SPEED,ADC_1);

    adc->setReference(ADC_REFERENCE::REF_EXT, ADC_0);
    adc->setReference(ADC_REFERENCE::REF_EXT, ADC_1);
    
    timer_between_adc = 0;
    timestamp = 0;
    timer_begin_adc = 0;
}


/**
*   @brief  Sets averaging interval
*   @param avg Number of samples to average at a given time
*   @return void
**/
void setAveraging(byte avg){
    desiredAvg = avg;
    adc->setResolution(desiredRes, ADC_0);
    adc->setResolution(desiredRes, ADC_1);
}

void setSampling(float sampkHz){
    between_adc_period = (unsigned int) ( 1.0 / sampkHz );
    SendByteOverHID(ADC_ADJ_SAMP,ADC_PRINT_KEY);
}

/**
*   @brief  Reads ADC data for a single channel or for all channels over finite time period and stores data to buffer
*
*   @param chan Selected ADC channel(s)
*   @return void
**/
void readADC(byte chan) {
  SendByteOverHID(ADC_READ,ADC_PRINT_KEY);
  clearADC();
  byte chanCount;
  if (chan == ALL_CHAN){
      chanCount = array_size;
  } else {
      chanCount = 1;
  }
  // Makes sure adc begins at the right time
  while(timer_begin_adc < begin_adc_period) {};
    // stores last current time
    previous_time = current_time;
    // grabs current running time
    current_time = timer_begin_adc;
    // reset timers
    timer_begin_adc = 0;
    //timer_between_adc = 0;

    read_start = timestamp; // save time of current reading
    diff = current_time - previous_time; // need to think about this a bit more
        
    // Record timestamp when returning to first channel in us
    //recent_values[0] = adc->analogRead(sensorPins[0]);
    // read remaining channels
    for (ii = 0; ii < chanCount; ii++)
    {
        // waste cycles until correct time, sets sampling rate of adc to great extent
        //while(timer_between_adc < between_adc_period) {}
        //timer_between_adc = 0; // reset timer
        recent_values[ii] = adc->analogRead(sensorPins[ii]);
    }
  // Causes garbage output if not casted properly
  float elapsed = (float) (timestamp - read_start);
  float sampFreq = (float) array_size / elapsed;
  SendByteOverHID(ADC_BUFFER, array_size,ADC_PRINT_ALL);
  SendByteOverHID(ADC_SAMPTIME, elapsed,ADC_PRINT_ALL);
  SendByteOverHID(ADC_SAMPFREQ, sampFreq,ADC_PRINT_ALL);
}

/**
*   @brief  Reads and sends out stored ADC reading data for particular ADC channel
*
*   @param chan Selected ADC channel
*   @return void
**/
// Send data through USB
void sendADCData(byte chan) {
  readADC(chan);
  SendByteOverHID(ADC_SEND,ADC_PRINT_KEY);
  byte chanCount;
  if (chan == ALL_CHAN){
      chanCount = array_size;
  } else {
      chanCount = 1;
  }

  buffer_adc[0] = (int)((read_start >> 24) & 0xFF);
  buffer_adc[1] = (int)((read_start >> 16) & 0xFF);
  buffer_adc[2] = (int)((read_start >> 8) & 0xFF);
  buffer_adc[3] = (int)((read_start & 0xFF));
  // double-check proper handling of *signed* int
  buffer_adc[4] = highByte(diff);
  buffer_adc[5] = lowByte(diff);
  for (jj = 0; jj < chanCount; jj++) {
      buffer_adc[jj * 2 + 6] = highByte(recent_values[jj]);
      buffer_adc[jj * 2 + 7] = lowByte(recent_values[jj]);
  }
  for (jj = 46; jj < 64; jj++) { 
      buffer_adc[jj] = 0; 
  }
  RawHID.send(buffer_adc, 0);

}

/**
*   @brief  Remove current entries from buffer array for ADC readings
*
*   @return void
**/
void clearADC() {
  SendByteOverHID(ADC_CLEAR_START,ADC_PRINT_ALL);
  for (unsigned int k = 0; k < channels; k++) {
    recent_values[k] = 0;
  }
  for (unsigned int k = 0; k < 64; k++){
    buffer_adc[k] = 0;
  }
  SendByteOverHID(ADC_CLEAR_FINISH,ADC_PRINT_ALL);
}

/**
*   @brief  Finds the average reading across all readings in a ADC DAQ recording
*
*   @return Average 16-bit ADC value wrt reference voltage
**/
float avgADC(byte chan) {
  unsigned long total = 0;
  for (unsigned int k = 0; k < avgAmt; k++) {
    total = total + adc->analogRead(sensorPins[chan]);;
  }
  float average = ((float) total) / avgAmt;
  return average;
}

/**
*   @brief  Routine for reading ADC readings from all ADC channels  
*
*   @return void
**/
void ADCRoutine(){
  digitalWrite(ledPin_adc, HIGH);   // LED off
  //SendByteOverHID(ADC_ROUTINE_START,ADC_PRINT_KEY);
  sendADCData(ALL_CHAN);
  SendByteOverHID(ADC_ROUTINE_FINISH,ADC_PRINT_KEY);
  digitalWrite(ledPin_adc, LOW);   // LED off
}

/**
*   @brief  Routine for recording ADC on a particular chaneel and sending over serial
*
*   @param chan Specified ADC channel
*   @return void
**/
void ADCChanTest(byte chan) {
  digitalWrite(ledPin_adc, HIGH);   // LED off
  SendByteOverHID(ADC_ROUTINE_START,ADC_PRINT_KEY);
  sendADCData(chan);
  SendByteOverHID(ADC_ROUTINE_FINISH,ADC_PRINT_KEY);
  digitalWrite(ledPin_adc, LOW);   // LED off
}
