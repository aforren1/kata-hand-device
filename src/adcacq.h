#ifndef ADCACQ_H
#define ADCACQ_H

typedef unsigned char byte;

void setupADC();

void setAveraging(byte avg);

void setSampling(float sampkHz);

void readADC(byte chan);

void readADCFull(byte chan);

void sendADCDataPlotter(byte chan);

// Send data through USB
void sendADCData(byte chan);

// Remove current entries from data array
void clearADC();

// Find average entry in data array
float avgADC(byte chan);

void ADCRoutine();

//  start test routine for recording ADC and sending over serial
void ADCChanTest(byte chan);

#endif
