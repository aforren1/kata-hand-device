/** 
 *  @file    calibration.cpp
 *  @author  Alexander Forrence, Jacob Carducci
 *  @date    06/15/2017
 *  @version 0.3
 *  
 *  @brief Calibration function definitions.
 *
 *  @section DESCRIPTION
 *  
 * All things dealing with calibration, including helpers.
 *
 **/

#include <ADC.h>
#include <stdint.h>
#include <array>
#include <i2c_t3.h>
#include "adcacq.h"
#include "Arduino.h"
#include "calibration.h"
#include "pga309.h"
#include "hidmisc.h"
#include "tca9548a.h"
#include "constants.h"

/** 
*   @brief  Top-level calibration routine for device.   
*
*   @return void
**/  
void RunCalibration(){
  SendByteOverHID(MISC_DIVIDER, MINIMAL_PRINT);
  SendByteOverHID(CALIB_NEW, MINIMAL_PRINT);
  byte chanCount = channels;
  byte maxIter = 1;
  float targetFrac = 0.5;
  for (byte chan = 0; chan < chanCount; chan++) {
    for (byte iter = 0; iter < maxIter; iter++){
        clearADC();
        SendByteOverHID(ADC_CHAN_SELECT, chan, ADC_PRINT_KEY);

        // Outputs current average and needed adjustment (may be 
        float avg = avgADC(chan);
        SendByteOverHID(ADC_AVG_INT, avg, ADC_PRINT_ALL);
        // Assume max level is 2^16 (int type)
        float avg_mV = (avg / intMax) * 1000 * Vcc;
        SendByteOverHID(ADC_AVG_V, avg_mV, ADC_PRINT_KEY);
        float input_mV = ReturnVoltageInDiff(avg_mV, chan);
        SendByteOverHID(CALIB_INPUT_V, input_mV, ADC_PRINT_KEY);
        float adjust_mV = avg_mV - (Vcc * targetFrac * 1000);
        SendByteOverHID(CALIB_ADJUST_V, -adjust_mV, ADC_PRINT_ALL);
        float error_mV = input_mV - 0;
        SendByteOverHID(CALIB_ERROR_IN_V, error_mV, ADC_PRINT_KEY);
        float error_out = 0;
        if (iter == 0){
            error_out = avg_mV - (Vcc * 1000 / 4);
        } else {
            error_out = adjust_mV;
        }
        SendByteOverHID(CALIB_ERROR_OUT_V, error_out, ADC_PRINT_KEY);
        // Enable intended PGA channel, may have an issue with it
        plexSelect(chan);

        SendByteOverHID(CALIB_SET_START, ADC_PRINT_KEY);

        // PART 1: MODIFY SENSITIVITY
        float desiredFrontGain = 32;
        float desiredOutputGain = 9;

        float coarse_mV_max = Vcc * 14 * 0.85;
        float excess_mV = 0;  
        if (error_mV > coarse_mV_max){
          excess_mV = error_mV - coarse_mV_max;
          error_mV = coarse_mV_max;
          SendByteOverHID(CALIB_EXCEED_COARSE, excess_mV, ADC_PRINT_ALL);
        } else if (error_mV < -coarse_mV_max){
          excess_mV = error_mV + coarse_mV_max;
          error_mV = -coarse_mV_max;
          SendByteOverHID(CALIB_EXCEED_COARSE, excess_mV, ADC_PRINT_ALL);
        }
      
        float desiredCoarseOffset = -error_mV;
        
        setSetting(0,chan,desiredFrontGain);
        setSetting(2,chan,desiredOutputGain);

        // Send to Front-End/Output Gain/Offset register 0x04
        uint16_t gainCMsg = writeToPGA(0x04,desiredFrontGain,desiredOutputGain,desiredCoarseOffset);
        SendByteOverHID(CALIB_CGAIN_MSG, gainCMsg, PGA_PRINT_KEY);

        // FIND REMAINING ERROR  
        uint16_t coarseMulti = gainCMsg & 0x000F;
        uint16_t coarseSign = (gainCMsg & 0x0010) >> 4;
        SendByteOverHID(CALIB_MULTI_MSG, coarseMulti, PGA_PRINT_ALL);
        SendByteOverHID(CALIB_SIGN_MSG, coarseSign, PGA_PRINT_ALL);
      
        if (coarseSign == 1){
          excess_mV = error_mV - coarseMulti*Vcc*0.85;
          setSetting(3,chan,-coarseMulti);
        } else {
          excess_mV = error_mV + coarseMulti*Vcc*0.85;
          setSetting(3,chan,coarseMulti);
        }
        SendByteOverHID(CALIB_ERROR_REMAIN, excess_mV, PGA_PRINT_KEY);
      
        // PART 2: MODIFY ZERO
        float desiredFineGain = 1;
        float desiredFineOffset = (targetFrac / (desiredFineGain * desiredOutputGain)) - desiredFrontGain * excess_mV / (Vcc*1000);
        setSetting(4,chan,desiredFineOffset);
        setSetting(1,chan,desiredFineGain);

        // Send to Fine Offset register 0x01 & send message
        uint16_t offsetMsg = writeToPGA(0x01,desiredFineOffset,0,0);
        uint16_t gainFMsg = writeToPGA(0x02,desiredFineGain,0,0);
        SendByteOverHID(CALIB_FGAIN_MSG, gainFMsg, PGA_PRINT_KEY);
        SendByteOverHID(CALIB_FOFF_MSG, offsetMsg, PGA_PRINT_KEY);
        // Check if communication failed
        printStatus(true,PGA_PRINT_KEY);
        printErrors(true,PGA_PRINT_KEY);
        printStatus(false,PGA_PRINT_ALL);
        printErrors(false,PGA_PRINT_ALL); 
        SendByteOverHID(MISC_DIVIDER,ADC_PRINT_KEY);
      }
  }
  SendByteOverHID(CALIB_FOFF_ADJ,ADC_PRINT_KEY);
  byte meetCtr = 0;
  for (byte j = 0; j < maxFineIter; j++){
    SendByteOverHID(CALIB_FOFF_ITER,PGA_PRINT_KEY);
    for (byte chan = 0; chan < chanCount; chan++) {
      SendByteOverHID(CALIB_FOFF_CHAN,PGA_PRINT_KEY);
      float GI = getSetting(0,chan);
      float step = 0.0001 / GI;
      float multiplier = 3;
      if (GI > 30) {
          multiplier = 5;
      }
      clearADC();
      float avg_fine = avgADC(chan);
      SendByteOverHID(ADC_AVG_INT,avg_fine,ADC_PRINT_ALL);
      // Assume max level is 2^16 (int type)
      float avg_mV_fine = (avg_fine / intMax) * 1000 * Vcc;
      SendByteOverHID(ADC_AVG_V,avg_mV_fine,ADC_PRINT_KEY);
      float adjust_mV_fine = avg_mV_fine - (Vcc * targetFrac * 1000);
      SendByteOverHID(ADC_ADJUST_V,-adjust_mV_fine,ADC_PRINT_ALL);
      float iter_step = 0;
      if (adjust_mV_fine > tol_mV) {
          SendByteOverHID(CALIB_FOFF_UP,PGA_PRINT_KEY);
          iter_step = -step * multiplier * adjust_mV_fine;
          meetCtr = 0;
      } else if (adjust_mV_fine < -tol_mV) {
          SendByteOverHID(CALIB_FOFF_DOWN,PGA_PRINT_KEY);
          iter_step = -step * multiplier * adjust_mV_fine;
          meetCtr = 0;
      } else {
          SendByteOverHID(CALIB_FOFF_TOL,PGA_PRINT_KEY);
          meetCtr++;
          SendByteOverHID(CALIB_FOFF_CHANGE, ADC_PRINT_KEY);
          continue;
      }
      float old_offset = getSetting(4,chan);
      float new_offset = old_offset + iter_step;
      SendByteOverHID(CALIB_FOFF_CURR,new_offset,PGA_PRINT_ALL);
      setSetting(4,chan,new_offset);
      plexSelect(chan);
      uint16_t offsetMsg = writeToPGA(0x01,new_offset,0,0);
      SendByteOverHID(CALIB_FOFF_CHANGE, ADC_PRINT_KEY);
    }
    SendByteOverHID(CALIB_FOFF_SETTLE, ADC_PRINT_KEY);
    long iterTimer = millis();
    while (millis() - iterTimer < settling_ms) {};
    if (meetCtr == chanCount){
      break;
    } else {
      meetCtr = 0;
    }
  }
}

/** 
*   @brief  Inverse function of transfer function from output to input voltage difference
*   
*   @param Vout Converted output voltage from ADC reading
*   @param currChan Current ADC channel with voltages
*   @return Expected differential voltage input from output
**/  
float ReturnVoltageInDiff(float Vout, byte currChan){
    float GI = getSetting(0,currChan);
    float GD = getSetting(1,currChan);
    float GO = getSetting(2,currChan);
    float Vc = getSetting(3,currChan);
    float Vf = getSetting(4,currChan) * Vcc * 1000;
    float Vin = (((Vout / (GD*GO)) - Vf) / GI) - Vc;
    return Vin;
}