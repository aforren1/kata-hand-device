#ifndef CALIBRATION_H
#define CALIBRATION_H

void AcquireSettingFromBuffer(int size, int *buffer);

void SetFineGain();

void ClearSerial();

void RunCalibration();

float ReturnVoltageInDiff(float Vout, byte currChan);

#endif
