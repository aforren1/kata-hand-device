#ifndef CONSTANTS_H
#define CONSTANTS_H

typedef unsigned char byte;

const float Vcc = 3.33;
const long intMax = 65535;

// These constants won't change:
// Delay constants

const int usDelay = 0;
const int quickDelay = 0;
const int shortDelay = 0;
const int longDelay = 500;

// 8 amplifier channels max per multiplex
const byte channels = 20;
const unsigned int sensorPins[] = {A12, A13, A16, A14, A17, A18, A19, A20, A21, A22, A0, A1, A5, A4, A3, A2, A6, A7, A8, A9};
const unsigned int array_size = sizeof(sensorPins)/sizeof(int); // 20 entries

const byte ALL_CHAN = 128;

const byte ledPin_calib = 0;        // Calibration process pin
const byte ledPin_adc = 1;        //  ADC process pin

// Memory
const uint8_t targetPGA = 0x40; // target Slave address of PGA309 (per devices)
const uint8_t plexA = 0x70; // target Slave address of Multiplex A
const uint8_t plexB = 0x71; // target Slave address of Multiplex B
const uint8_t plexC = 0x72; // target Slave address of Multiplex C

const byte bufferLen = 64;

const byte NO_PRINT = 0;
const byte MINIMAL_PRINT = 1;
const byte ADC_PRINT_KEY = 2;
const byte PGA_PRINT_KEY = 3;
const byte PLEX_PRINT_KEY = 4;
const byte ADC_PRINT_ALL = 5;
const byte PGA_PRINT_ALL = 6;
const byte PLEX_PRINT_ALL = 7;
const byte ALL_PRINT = 8;

// Data array initialization (try to control for 1kHz/channel)
const unsigned int avgAmt = 1000;
const unsigned int timeDiv = channels;

const byte maxFineIter = 10;
const float tol_mV = 20;
//const byte startPrint = PGA_PRINT_ALL;
const byte startPrint = NO_PRINT;
const int settling_ms = 100;


const unsigned int begin_adc_period = 1000; // == sampling period (in this case, 1000Hz)

// Error / Status Codes
// Statuses are in range 0x00 to 0x9F (have a numerical top nibble)
// Errors are in range 0xA0 to 0xFF (have an alphabetical top nibble)

// Status
const byte MSG_EMPTY = 0x00;
const byte DEVICE_INIT = 0x01;
const byte DEVICE_SETUP = 0x02;

const byte ADC_READ = 0x10;
const byte ADC_BUFFER = 0x11;
const byte ADC_SAMPTIME = 0x12;
const byte ADC_SAMPFREQ = 0x13;
const byte ADC_SEND = 0x14;
const byte ADC_CLEAR_START = 0x15;
const byte ADC_CLEAR_FINISH = 0x16;
const byte ADC_ROUTINE_START = 0x17;
const byte ADC_ROUTINE_FINISH = 0x18;
const byte ADC_CHAN_SELECT = 0x19;
const byte ADC_AVG_INT = 0x1A;
const byte ADC_AVG_V = 0x1B;
const byte ADC_ADJUST_V = 0x1C;
const byte ADC_ADJ_SAMP = 0x1D;

const byte PGA_FRONT_GAIN_SET = 0x20;
const byte PGA_FINE_GAIN_SET = 0x21;
const byte PGA_OUT_GAIN_SET = 0x22;
const byte PGA_COARSE_OFF_SET = 0x23;
const byte PGA_FINE_OFF_SET = 0x24;

const byte PGA_TEMP_ADC_READ = 0x25;
const byte PGA_FINE_OFF_READ = 0x26;
const byte PGA_FINE_GAIN_READ = 0x27;
const byte PGA_REF_CTRL_READ = 0x28;
const byte PGA_COARSE_OUT_READ = 0x29;
const byte PGA_CONFIG_LIM_READ = 0x2A;
const byte PGA_TEMP_CTRL_READ = 0x2B;
const byte PGA_OUT_ENABLE_READ = 0x2C;
const byte PGA_FAULT_READ = 0x2D;
const byte PGA_CURR_REG = 0x2E;

const byte PGA_SLAVE_SEND = 0x2F;
const byte PGA_WRITE_ADDR = 0x30;
const byte PGA_RAW_DATA = 0x31;
const byte PGA_RAW_TOP = 0x32;
const byte PGA_RAW_BOTTOM = 0x33;
const byte PGA_SLAVE_RECV = 0x34;
const byte PGA_READ_ADDR = 0x35;
const byte PGA_AVAIL_BYTE = 0x36;
const byte PGA_TEMP = 0x37;
const byte PGA_ERROR = 0x38;

const byte PLEX_CURR_DEVICE = 0x40;
const byte PLEX_CURR_PGA = 0x41;
const byte PLEX_MANUAL_CHANGE = 0x42;
const byte PLEX_MANUAL_FINISH = 0x43;
const byte PLEX_MANUAL_CLEAR = 0x44;
const byte PLEX_SEND_DEVICE = 0x45;
const byte PLEX_REG_DESIRED = 0x46;
const byte PLEX_SEND_VERIFY = 0x47;
const byte PLEX_REG_NEW = 0x48;

const byte CALIB_NEW = 0x50;
const byte CALIB_INPUT_V = 0x51;
const byte CALIB_ADJUST_V = 0x52;
const byte CALIB_ERROR_IN_V = 0x53;
const byte CALIB_ERROR_OUT_V = 0x54;
const byte CALIB_SET_START = 0x55;
const byte CALIB_EXCEED_COARSE = 0x56;

const byte CALIB_CGAIN_MSG = 0x57;
const byte CALIB_MULTI_MSG = 0x58;
const byte CALIB_SIGN_MSG = 0x59;
const byte CALIB_ERROR_REMAIN = 0x5A;
const byte CALIB_FGAIN_MSG = 0x5B;

const byte CALIB_FOFF_ADJ = 0x5C;
const byte CALIB_FOFF_MSG = 0x5D;
const byte CALIB_FOFF_TOL = 0x5E;
const byte CALIB_FOFF_UP = 0x5F;
const byte CALIB_FOFF_DOWN = 0x60;
const byte CALIB_FOFF_CURR = 0x61;
const byte CALIB_FOFF_ITER = 0x62;
const byte CALIB_FOFF_CHAN = 0x63;
const byte CALIB_FOFF_CHANGE = 0x64;
const byte CALIB_FOFF_SETTLE = 0x65;

const byte INPUT_RAW_READ = 0x70;
const byte INPUT_WAIT = 0x71;
const byte INPUT_SEND = 0x72;
const byte INPUT_SEND_ADDR = 0x73;
const byte INPUT_RECV = 0x74;
const byte INPUT_SEND_SLAVE = 0x75;
const byte INPUT_RECV_SLAVE = 0x76;
const byte INPUT_NO_ERROR = 0x77;
const byte INPUT_ADJ_GAINC = 0x78;
const byte INPUT_ADJ_GAINF = 0x79;

const byte MISC_DIVIDER = 0x9F;

// Errors
  
const byte ADC_CHAN_INVALID = 0xB0;
  
const byte PGA_SET_INVALID = 0xC0;
const byte PGA_READ_UNKN = 0xC1;
const byte PGA_READ_ONLY = 0xC2;

const byte INPUT_TIMEOUT = 0xF0;
const byte INPUT_NO_ACKL_ADDR = 0xF1;
const byte INPUT_NO_ACKL_DATA = 0xF2;
const byte INPUT_ARB_LOST = 0xF3;
const byte INPUT_ERROR_UNKN = 0xF4;
const byte INPUT_LONG_DATA = 0xF5;

#endif