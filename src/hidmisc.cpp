/** 
 *  @file    serialmisc.cpp
 *  @author  Jacob Carducci
 *  @date    06/15/2017
 *  @version 0.2
 *  
 *  @brief General serial-related function definitions.
 *
 *  @section DESCRIPTION
 *  
 * All things dealing with serial commmuncation in general, including helpers.
 *
 **/

#include <ADC.h>
#include <stdint.h>
#include <i2c_t3.h>
#include "adcacq.h"
#include "Arduino.h"
#include "calibration.h"
#include "pga309.h"
#include "hidmisc.h"
#include "constants.h"
#include "tca9548a.h"

// Three levels of printing
// NO_PRINT: no printing what-so-ever
// ADC_PRINT: printing of ADC only
// PGA_PRINT: printing of ADC and PGA only
// PLEX_PRINT: print ADC, PGA, and key plex info
// ALL_PRINT: print everything
byte printLevel = startPrint;

byte buffer_tx[64];

bool bIsMessaging = false;

bool checkMessaging(){
  return bIsMessaging;
}

void setMsgMode(bool msgIntent){
  bIsMessaging = msgIntent;
}

/**
*   @brief Parses user input and formats it for use with amplifier setting adjustment
*
*   @param str Buffered string containing user-specified input
*   @return Calculated hex value sent to PGA (assuming no calibration or incorrect messages)
**/
void userInput(byte *buffer){
  switch (buffer[0]){
    // DAQ Mode
    case 'd':
    {
    setMsgMode(false);
      break;
    }
    // Messaging Mode
    case 'm':
    {
    setMsgMode(true);
    byte print = buffer[1];
    setPrintLevel(print);      
      break;
    }
    // Recalibration Mode
    case 'c':
    {
      setMsgMode(true);
      RunCalibration();
      break;
    }
    // Gain Adjustment Mode
    case 'g':
    {
      setMsgMode(true);
      // Would user input be a float though? Like, have decimal points and everything?
      uint16_t gain = concatBytes(buffer[1],buffer[2]);
      byte channel = buffer[3];
      UserGainToSystemGains(gain,channel);
      RunCalibration();
      break;
    }
    // Sampling Adjustment Mode
    case 's':
      {
      setMsgMode(true);
      float samp = (float) buffer[1];
      setSampling(samp);
      break;
     }
    // Message asking for correct input
    default:
      break;
  }
}

/**
*   @brief  Combines two 8-bit hex values into a single 16-bit hex
*   @details Assumes big endianness (i.e. value in first addr goes to MSB, assuming value is in byte1)
*   @param byte1 First byte
*   @param byte2 Second byte
*   @return Combined 2-byte message
**/
uint16_t concatBytes(uint8_t byte1, uint8_t byte2) {
  uint16_t twoByte;
  twoByte = byte1;
  twoByte = twoByte << 8;
  twoByte |= byte2;
  SendByteOverHID(INPUT_RAW_READ,PGA_PRINT_KEY);
  return twoByte;
}

/**
*   @brief  Takes single 16-bit hex and splits it into two 8-bit hex values
*
*   @param *pWrite Pointer to array that will store 8-bit hex value outputs
*   @param twoByte Input single 16-bit hex
*   @return void
**/
void splitTwoByte(uint8_t *pWrite, uint16_t twoByte) {
  // Clear array
  pWrite[1] = 0x00;
  pWrite[0] = 0x00;
  uint8_t botByte = (uint8_t) (twoByte & 0xFF);
  uint8_t topByte = (uint8_t) ((twoByte >> 8) & 0xFF);
  pWrite[1] = topByte;
  pWrite[0] = botByte;
}

/**
*   @brief  Prints I2C status emerging from device communication
*
*   @param keyInfoOnly Show only non-ready status on true; show everything including waiting/ready status on false
*   @return void
**/
void printStatus(bool keyInfoOnly, byte print)
{
  switch (Wire2.status())
  {
    case I2C_WAITING:
      if (!keyInfoOnly) SendByteOverHID(INPUT_WAIT, print);
      break;
    case I2C_SENDING:
      SendByteOverHID(INPUT_SEND, print);
      break;
    case I2C_SEND_ADDR:
      SendByteOverHID(INPUT_SEND_ADDR,print);
      break;
    case I2C_RECEIVING:
      SendByteOverHID(INPUT_RECV,print);
      break;
    case I2C_TIMEOUT:
      SendByteOverHID(INPUT_TIMEOUT,print);
      break;
    case I2C_ADDR_NAK:
      SendByteOverHID(INPUT_NO_ACKL_ADDR,print);
      break;
    case I2C_DATA_NAK:
      SendByteOverHID(INPUT_NO_ACKL_DATA,print);
      break;
    case I2C_ARB_LOST:
      SendByteOverHID(INPUT_ARB_LOST,print);
      break;
    case I2C_SLAVE_TX:
      SendByteOverHID(INPUT_SEND_SLAVE,print);
      break;
    case I2C_SLAVE_RX:
      SendByteOverHID(INPUT_RECV_SLAVE,print);
      break;
    default:
      SendByteOverHID(INPUT_ERROR_UNKN,print);
      break;
  }
}

/**
*   @brief  Prints I2C errors emerging from device communication
*
*   @param keyInfoOnly Show only errors on true; show everything including healthy status on false
*   @return void
**/
void printErrors(bool keyInfoOnly, byte print)
{
  switch (Wire2.getError())
  {
    case 0:
      if (!keyInfoOnly) SendByteOverHID(INPUT_NO_ERROR,print);
      break;
    case 1:
      SendByteOverHID(INPUT_LONG_DATA,print);
      break;
    case 2:
      SendByteOverHID(INPUT_NO_ACKL_ADDR,print);
      break;
    case 3:
      SendByteOverHID(INPUT_NO_ACKL_DATA,print);
      break;
    case 4:
      SendByteOverHID(INPUT_LONG_DATA,print);
      break;
    default:
      SendByteOverHID(INPUT_ERROR_UNKN,print);
      break;
  }
}

/**
*   @brief  Sets how much printing occurs and what is printed
*   @param newLevel The new print level desired
*   @return void
**/
void setPrintLevel(byte newLevel){
    printLevel = newLevel;
}

/**
*   @brief  Gets current degree of printing
*
*   @return Print level set for serial monitor
**/
byte getPrintLevel(){
   return printLevel;
}

void UserGainToSystemGains(uint16_t user_gain, byte chan){
  float GO, GD, GI;
  GO = 9;
  float base_gain = user_gain / GO;
  byte front_gain;
  if (base_gain <= 4.1){
    front_gain = 0x00;
    GI = 4;
  } else if (base_gain <= 8.1) {
    front_gain = 0x01;
    GI = 8;
  } else if (base_gain <= 16.1) {
    front_gain = 0x02;
    GI = 16;
  } else if (base_gain <= 23.37) {
    front_gain = 0x03;
    GI = 23.27;
  } else if (base_gain <= 32.1) {
    front_gain = 0x04;
    GI = 32;
  } else if (base_gain <= 42.77) {
    front_gain = 0x05;
    GI = 42.67;
  } else if (base_gain <= 64.1) {
    front_gain = 0x06;
    GI = 64;
  } else {
    front_gain = 0x07; // Gain is 128
    GI = 128;
  }
  GD = base_gain / front_gain;

  // Set proper gains
  setSetting(0, chan, GI);
  setSetting(1, chan, GD);
  setSetting(2, chan, GO);
  float CO = getSetting(3,chan);

  // switch to proper PGA
  plexSelect(chan);
  // Writes gains, but retains coarse offset
  uint16_t gainCMsg = writeToPGA(0x04,GI,GO,CO);
  uint16_t gainFMsg = writeToPGA(0x02,GD,0,0);
  SendByteOverHID(INPUT_ADJ_GAINC,gainCMsg,PGA_PRINT_KEY);
  SendByteOverHID(INPUT_ADJ_GAINF,gainCMsg,PGA_PRINT_KEY);
}

/**
*   @brief  Sets how much printing occurs and what is printed
*   @param newLevel The new print level desired
*   @return void
**/

void SendByteOverHID(byte errorCode, byte print){
  if (bIsMessaging && printLevel >= print){
    buffer_tx[0] = 'm';
    buffer_tx[1] = errorCode;
    for (byte i = 2; i < 64; i++){
      buffer_tx[i] = 0;
    }
    RawHID.send(buffer_tx,1);
  }
}

void SendByteOverHID(byte errorCode, uint8_t val, byte print){
  if (bIsMessaging && printLevel >= print){
    buffer_tx[0] = 'm';
    buffer_tx[1] = errorCode;
    buffer_tx[2] = (byte) val;
    for (byte i = 3; i < 64; i++){
      buffer_tx[i] = 0;
    }
    RawHID.send(buffer_tx,1); 
  }
}

void SendByteOverHID(byte errorCode, unsigned int val, byte print){
  if (bIsMessaging && printLevel >= print){
    buffer_tx[0] = 'm';
    buffer_tx[1] = errorCode;
    buffer_tx[2] = (byte) ((val >> 8) & 0xFF);
    buffer_tx[3] = (byte) (val & 0xFF);
    for (byte i = 4; i < 64; i++){
      buffer_tx[i] = 0;
    }
    RawHID.send(buffer_tx,1);    
  }
}

void SendByteOverHID(byte errorCode, uint16_t val, byte print){
  if (bIsMessaging && printLevel >= print){
    buffer_tx[0] = 'm';
    buffer_tx[1] = errorCode;
    buffer_tx[2] = (byte) ((val >> 8) & 0xFF);
    buffer_tx[3] = (byte) (val & 0xFF);
    for (byte i = 4; i < 64; i++){
      buffer_tx[i] = 0;
    }
    RawHID.send(buffer_tx,1);
  }
}

/**
*   @brief  Sets how much printing occurs and what is printed
*   @param newLevel The new print level desired
*   @return void
**/
void SendByteOverHID(byte errorCode, long val, byte print){
  if (bIsMessaging && printLevel >= print){
    buffer_tx[0] = 'm';
    buffer_tx[1] = errorCode;
    buffer_tx[2] = (byte) ((val >> 24) & 0xFF);
    buffer_tx[3] = (byte) ((val >> 16) & 0xFF);
    buffer_tx[4] = (byte) ((val >> 8) & 0xFF);
    buffer_tx[5] = (byte) (val & 0xFF);
    for (byte i = 6; i < 64; i++){
      buffer_tx[i] = 0;
    }
    RawHID.send(buffer_tx,1);
  }
}
      
void SendByteOverHID(byte errorCode, unsigned long val, byte print){
  if (bIsMessaging && printLevel >= print){
    buffer_tx[0] = 'm';
    buffer_tx[1] = errorCode;
    buffer_tx[2] = (byte) ((val >> 24) & 0xFF);
    buffer_tx[3] = (byte) ((val >> 16) & 0xFF);
    buffer_tx[4] = (byte) ((val >> 8) & 0xFF);
    buffer_tx[5] = (byte) (val & 0xFF);
    for (byte i = 6; i < 64; i++){
      buffer_tx[i] = 0;
    }
    RawHID.send(buffer_tx,1);
  }
}

/**
*   @brief  Sets how much printing occurs and what is printed
*   @param newLevel The new print level desired
*   @return void
**/
void SendByteOverHID(byte errorCode, float val, byte print){
  if (bIsMessaging && printLevel >= print){
    buffer_tx[0] = 'm';
    buffer_tx[1] = errorCode;

    byte temp[4];
    union {
      float float_variable;
      byte temp_array[4];
    } u;
    u.float_variable = val;
    memcpy(temp,u.temp_array,4);
    for (byte j = 0; j < 4; j++){
      buffer_tx[2+j] = temp[j];
    }

    for (byte i = 6; i < 64; i++){
      buffer_tx[i] = 0;
    }
    RawHID.send(buffer_tx,1); 
  }
}