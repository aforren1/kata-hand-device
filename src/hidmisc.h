#ifndef HIDMISC_H
#define HIDMISC_H

typedef unsigned char byte;

bool checkMessaging();

void setMsgMode(bool msgIntent);

void userInput(byte *buffer);

uint16_t concatBytes(uint8_t byte1, uint8_t byte2);

void splitTwoByte(uint8_t *pWrite, uint16_t twoByte);

void viewSettings();

void printStatus(bool keyInfoOnly, byte print);

void printErrors(bool keyInfoOnly, byte print);

void setPrintLevel(byte newLevel);

byte getPrintLevel();

void UserGainToSystemGains(uint16_t user_gain, byte chan);

void SendByteOverHID(byte errorCode);

void SendByteOverHID(byte errorCode, byte print);

void SendByteOverHID(byte errorCode, uint8_t val, byte print);

void SendByteOverHID(byte errorCode, unsigned int val, byte print);

void SendByteOverHID(byte errorCode, unsigned long val, byte print);

void SendByteOverHID(byte errorCode, long val, byte print);

void SendByteOverHID(byte errorCode, uint16_t val, byte print);

void SendByteOverHID(byte errorCode, float val, byte print);

#endif
