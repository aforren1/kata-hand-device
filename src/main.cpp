/** 
 *  @file    main.cpp
 *  @author  Alexander Forrence, Jacob Carducci
 *  @date    06/15/2017
 *  @version 0.3
 *  
 *  @brief Main routine for Kata hand force device.
 *
 *  @section DESCRIPTION
 *  
 * Let's aim to keep this top-level as barebones as possible.
 *
 **/

#include <ADC.h>
#include <stdint.h>
#include <i2c_t3.h>
#include "adcacq.h"
#include "Arduino.h"
#include "calibration.h"
#include "pga309.h"
#include "hidmisc.h"
#include "tca9548a.h"
#include "constants.h"

byte buffer_rx[64]; ///< Buffer used to store data leaving via USB.

// receive status
int n;

String inputBuffer = ""; // holding incoming data

/**
*   @brief Obligatory setup routine (runs once when the device is powered on).
*   
*   @return void
**/
void setup() 
{
  // Set adc pins to input
  for (byte i = 0; i < channels; i++){
    pinMode(sensorPins[i],INPUT);
  }
  // Set led pins to output
  pinMode(LED_BUILTIN, OUTPUT);   // LED
  pinMode(ledPin_calib, OUTPUT);   // LED
  pinMode(ledPin_adc, OUTPUT);   // LED
  digitalWrite(LED_BUILTIN, LOW); // LED off
  digitalWrite(ledPin_calib, LOW);   // LED off
  digitalWrite(ledPin_adc, LOW);   // LED off
    
  // Setup for Master mode, pins 3_4, external pullups, 400kHz, 200ms default timeout, Teensy sets 0x00 address, but does not matter as master
  Wire2.begin(I2C_MASTER, 0x00, I2C_PINS_3_4, I2C_PULLUP_EXT, 400000);
  Wire2.setDefaultTimeout(200000); // 200ms
    
  // Ready delay
  setMsgMode(false);
  SendByteOverHID(DEVICE_INIT, MINIMAL_PRINT);
  
  // SETUP BEGINS
  // Begin one-off test (Comment out this block to exclude additional hardware)
  setupADC();
  setupPGA();
  plexReset();
  plexSelect(0);
  //plexRoutine();
  //ADCRoutine();
  RunCalibration();
  //ADCRoutine();
  //viewSettings();
  SendByteOverHID(DEVICE_SETUP, MINIMAL_PRINT);
}

/**
*   @brief Obligatory loop routine (runs forever).
*
*   @return void
**/
void loop() 
{    
  // check for messages from the host machine (i.e. a signal to enter calibration routine)
  
  n = RawHID.recv(buffer_rx, 0);
  if (n > 0) userInput(buffer_rx);
  
  
  // If sufficient time elapsed, acquire a sample and immediately send to host (Comment out to exclude additional hardware)
  if (!checkMessaging()) ADCRoutine();
}