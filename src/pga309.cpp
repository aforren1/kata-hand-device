/** 
 *  @file    pga309.cpp
 *  @author  Jacob Carducci
 *  @date    06/15/2017
 *  @version 0.2
 *  
 *  @brief Programmable amplifier function definitions.
 *
 *  @section DESCRIPTION
 *  
 * All things dealing with the PGA309 amplifiers, including helpers.
 *
 **/

#include <stdint.h>
#include <i2c_t3.h>
#include "Arduino.h"
#include "hidmisc.h"
#include "pga309.h"
#include "tca9548a.h"
#include "constants.h"

// State arrays for each setting for all amplifiers; indices correspond to sensorPins[]
float frontGains[channels];
float fineGains[channels];
float outGains[channels];
float coarseSets[channels];
float fineSets[channels];
uint8_t writeArray[2];
uint16_t rawTwoByte;
uint8_t byte1; uint8_t byte2;

byte buffer_setting[64];

/** 
*   @brief  Sets setting arrays to default power-on characteristics of amplifier for all channels
*   
*   @return void
**/  
void setupPGA(){
  for (byte chan = 0; chan < channels; chan++){
    frontGains[chan] = 4;
    fineGains[chan] = 0.5;
    outGains[chan] = 2;
    coarseSets[chan] = 0;
    fineSets[chan] = 0.25;
    
    plexSelect(chan);
    
    // Decrease clock noise
    //uint16_t clkMsg = writeToPGA(0x05,0,0,0);    
  }  
}

/** 
*   @brief Sets local memory of a setting's value for a given amplifier   
*   
*   @param setting Selects amplifying characteristic of amplifier (0: front gain; 1: fine gain; 2: output gain; 3: coarse offset; 4: fine offset)
*   @param chan Current ADC channel/amplifier with characteristics and amplifier
*   @param value Desired level of characteristic setting
*   @return void
**/  
void setSetting(byte setting, byte chan, float value){
  switch(setting){
    case 0:
      frontGains[chan] = value;
      SendByteOverHID(PGA_FRONT_GAIN_SET, PGA_PRINT_ALL);
      break;
    case 1:
      fineGains[chan] = value;
      SendByteOverHID(PGA_FINE_GAIN_SET, PGA_PRINT_ALL);
      break;
    case 2:
      outGains[chan] = value;
      SendByteOverHID(PGA_OUT_GAIN_SET, PGA_PRINT_ALL);
      break;
    case 3:
      coarseSets[chan] = value;
      SendByteOverHID(PGA_COARSE_OFF_SET, PGA_PRINT_ALL);
      break;
    case 4:
      fineSets[chan] = value;
      SendByteOverHID(PGA_FINE_OFF_SET, PGA_PRINT_ALL);
      break;
    default:
      SendByteOverHID(PGA_SET_INVALID, PGA_PRINT_ALL);
    break;
  }    
}

/** 
*   @brief  Gets local memory of a setting's value for a given amplifer 
*   
*   @param setting Selects amplifying characteristic of amplifier (0: front gain; 1: fine gain; 2: output gain; 3: coarse offset; 4: fine offset)
*   @param chan Current ADC channel/amplifier with setting
*   @return Setting value
**/  
float getSetting(byte setting, byte chan){
  float msg = -1;
  switch(setting){
    case 0:
      msg = frontGains[chan];
      break;
    case 1:
      msg = fineGains[chan];
      break;
    case 2:
      msg = outGains[chan];
      break;
    case 3:
      msg = coarseSets[chan];
      break;
    case 4:
      msg = fineSets[chan];
      break;
    default:
      SendByteOverHID(PGA_SET_INVALID, PGA_PRINT_ALL);
    break;
  }
  return msg;
}

/**
*   @brief  Prints out stored values of all amplifiers regarding all settings (i.e. front-end gain, fine gain, output gain, coarse offset, fine offset, etc.)
*
*   @return void
**/
// buffer_tx not declared in this module
void viewSettings(){
  for (byte i = 0; i < 5; i++){
    for (byte j = 0; j < channels; j++){
      buffer_setting[j] = getSetting(i,j);      
    }
    for (byte k = channels + 1; k < 64; k++){
      // 5 separate buffer sends
      buffer_setting[k] = 0;
    }
    RawHID.send(buffer_setting,1);    
  }
}

/**
*   @brief  Access a given PGA amplifier through I2C and read/write settings 
*
*   @param addr The desired setting register to access the PGA with
*   @param isRead Set to true if reading settings; false if writing settings
*   @return void
**/
void accessRegister(uint8_t addr, bool isRead) {
  digitalWrite(LED_BUILTIN, HIGH);  // LED on
  // Transmit to Slave

  if (addr > 0x08) {
    addr = 0x08;
  }
  switch (addr) {
    case 0x00:
      SendByteOverHID(PGA_TEMP_ADC_READ,PGA_PRINT_KEY);
      break;
    case 0x01:
      SendByteOverHID(PGA_FINE_OFF_READ,PGA_PRINT_KEY);
      break;
    case 0x02:
      SendByteOverHID(PGA_FINE_GAIN_READ,PGA_PRINT_KEY);
      break;
    case 0x03:
      SendByteOverHID(PGA_REF_CTRL_READ,PGA_PRINT_KEY);
      break;
    case 0x04:
      SendByteOverHID(PGA_COARSE_OUT_READ,PGA_PRINT_KEY);
      break;
    case 0x05:
      SendByteOverHID(PGA_CONFIG_LIM_READ,PGA_PRINT_KEY);
      break;
    case 0x06:
      SendByteOverHID(PGA_TEMP_CTRL_READ,PGA_PRINT_KEY);
      break;
    case 0x07:
      SendByteOverHID(PGA_OUT_ENABLE_READ,PGA_PRINT_KEY);
      break;
    case 0x08:
      SendByteOverHID(PGA_FAULT_READ,PGA_PRINT_KEY);
      break;
    default:
      SendByteOverHID(PGA_READ_UNKN,PGA_PRINT_KEY);
  }
    
  uint8_t targetPlex = getPlexDevice();
  uint8_t currPlex = getPlexChan();
  SendByteOverHID(PLEX_CURR_DEVICE,targetPlex,PLEX_PRINT_KEY);
  SendByteOverHID(PLEX_CURR_PGA,currPlex,PGA_PRINT_KEY);
  SendByteOverHID(PGA_CURR_REG,addr,PGA_PRINT_KEY);
  // Initialize Tx buffer
  if (isRead) {
    readFromPGA(addr);
  } else if (!isRead && (addr != 0x00) && (addr != 0x08)) {
    // Empty for now; originally was for push-button based testing
  } else {
     SendByteOverHID(PGA_READ_ONLY,PGA_PRINT_KEY);
  }

  // Check if communication failed
  printStatus(true,MINIMAL_PRINT);
  printErrors(true,MINIMAL_PRINT);
  printStatus(false,MINIMAL_PRINT);
  printErrors(false,MINIMAL_PRINT);
  
  digitalWrite(LED_BUILTIN, LOW);   // LED off
}

/**
*   @brief  Helper function for writing desired setting to PGA
*
*   @param addr The desired setting register to access the PGA with
*   @param value# Values needed for specifying particular setting level
*   @return Resulting 16-bit hex value for setting and value
**/
uint16_t writeToPGA(uint8_t addr, float value1, float value2, float value3) {
  // Print message to register to modify amp behavior
  SendByteOverHID(PGA_SLAVE_SEND,PGA_PRINT_ALL);
  Wire2.beginTransmission(targetPGA);   // Slave address
  Wire2.write(addr);
  SendByteOverHID(PGA_WRITE_ADDR,PGA_PRINT_ALL);
  // Has to be two addresses
  // Address 1-8, then address 9-16
  rawTwoByte = writeSelect(addr, value1, value2, value3);
  splitTwoByte(writeArray, rawTwoByte);
  // Could be falsely communicating
  Wire2.write(writeArray, 2); // transmits two bytes from an array
  // Transmit Tx buffer
  Wire2.endTransmission();           // Transmit to Slave

  SendByteOverHID(PGA_RAW_DATA,rawTwoByte,PGA_PRINT_ALL);
  SendByteOverHID(PGA_RAW_TOP,writeArray[1],PGA_PRINT_ALL);
  SendByteOverHID(PGA_RAW_BOTTOM,writeArray[0],PGA_PRINT_ALL);

  return rawTwoByte;
}

/**
*   @brief  Selects an internal register to write to
*
*   @param addr The desired setting register to access the PGA with
*   @param value# Values needed for specifying particular setting level
*   @return Resulting 16-bit hex value for setting and value
**/
// 
uint16_t writeSelect(uint8_t addr, float value1, float value2, float value3) {
  uint16_t writeMsg = 0x0000;
  switch (addr) {
    case 0x01:
      writeMsg = fineOffsetAdjust(value1);
      break;
    case 0x02:
      writeMsg = fineGainAdjust(value1);
      break;
    case 0x03:
      writeMsg = refCtrlLinear();
      break;
    case 0x04:
      writeMsg = PGAOffsetGain(value1, value2, value3);
      break;
    case 0x05:
      writeMsg = PGAConfigLimit();
      break;
    case 0x06:
      writeMsg = TempADCCtrl();
      break;
    case 0x07:
      writeMsg = OutEnableCounterCtrl();
      break;
    default:
      writeMsg = fineOffsetAdjust(value1);
    break;
  }
  return writeMsg;
}

/**
*   @brief   Helper function for reading desired setting to PGA
*
*   @param addr The desired setting register to access the PGA with
*   @return void
**/
void readFromPGA(uint8_t addr) {
  byte numBytes = 0;
  SendByteOverHID(PGA_SLAVE_RECV,PGA_PRINT_ALL);
  SendByteOverHID(PGA_READ_ADDR,addr,PGA_PRINT_ALL);
  byte RedundancyReads = 5;
  for (byte i = 0; i < RedundancyReads; i++){
      Wire2.beginTransmission(targetPGA);   // Slave address
      Wire2.write(addr);
      // Read from Slave
      Wire2.requestFrom(targetPGA, 2, I2C_NOSTOP);
      numBytes = Wire2.available();
      while (Wire2.available()) {
        byte2 = Wire2.readByte();
        byte1 = Wire2.readByte();
      }
      // Transmit Tx buffer (unneccessary for reading?)
      Wire2.endTransmission();           // Transmit to Slave
  }
  SendByteOverHID(PGA_AVAIL_BYTE,numBytes,PGA_PRINT_ALL);
  // Temperature reading
  if (addr == 0x00) {
    float temp = tempConvert(byte1, byte2);
    SendByteOverHID(PGA_TEMP,temp,MINIMAL_PRINT);
    // Error code reading
  } else {
    uint16_t errorCode = concatBytes(byte1, byte2);
    SendByteOverHID(PGA_ERROR,errorCode,PGA_PRINT_ALL);
  }
  byte1 = 0x00;
  byte2 = 0x00;
}

/**
*   @brief  Computes setting hex needed to represent intended fine offset
*
*   @param frac Desired fine offset as a fraction of the reference voltage
*   @return Resulting 16-bit hex value for setting and value
**/
uint16_t fineOffsetAdjust(float frac) {
  float ZD_float = frac * intMax;
  uint16_t ZD = (uint16_t) ZD_float;
  return ZD;
}

/**
*   @brief  Computes setting hex needed to represent intended fine gain
*
*   @param targetG Desired fine gain
*   @return Resulting 16-bit hex value for setting and value
**/
uint16_t fineGainAdjust(float targetG) {
  float GD_float = (targetG - 0.33333333) * 1.5 * intMax;
  uint16_t GD = (uint16_t) GD_float;
  return GD;
}

/**
*   @brief  Computes setting hex needed to represent intended reference control and linearization
*   @details Not utilized, but coded for potential future use
*
*   @return Resulting 16-bit hex value for setting and value
**/
uint16_t refCtrlLinear() {
  uint16_t result;

  uint8_t RFB = 0x00;

  // Linear Adjust, V_exc gain select
  // 0x00 : -0.166 V_fb to +0.166 V_fb lineariz. DAC; 0.83 V_ref of V_exc gain
  // 0x01 : -0.124 V_fb to +0.124 V_fb lineariz. DAC; 0.52 V_ref of V_exc gain
  uint8_t EXS = 0x00;

  // Enable V_exc
  uint8_t EXEN = 0x00;

  // Select internal V_ref (0-4.096V, 1-2.5V)
  uint8_t RS = 0x00;

  // Enable internal V_ref
  uint8_t REN = 0x00;

  float FSR;
  if (EXS == 0x01) {
    FSR = 0.166;
  } else {
    FSR = 0.124;
  }

  float desiredFBRatio = 0.04183; // ratio
  int8_t LD = (int8_t) (desiredFBRatio / (FSR / 127));

  // Starts with 4 bits
  result = RFB; result = result << 1;
  result |= EXS; result = result << 1;
  result |= EXEN; result = result << 1;
  result |= RS; result = result << 1;
  result |= REN; result = result << 8;
  result |= LD;

  return result;
}

/**
*   @brief  Computes setting hex needed to represent intended front-end and output gains, and coarse offset
*
*   @param dGainF Desired front-end gain
*   @param dGainO Desired output gain
*   @param dOffset Desired coarse offset
*   @return Resulting 16-bit hex value for setting and value
**/
uint16_t PGAOffsetGain(float dGainF, float dGainO, float dOffset) {
  // dOffset should be in mV
  uint16_t result;

  uint8_t OWD = 0x01; // Disable PRG pin

  uint8_t GO, GI;

  // Output Amp gain table
  // 0x00 - 2
  // 0x01 - 2.4
  // 0x02 - 3
  // 0x03 - 3.6
  // 0x04 - 4.5
  // 0x05 - 6
  // 0x06 - 9
  // 0x07 - Disable internal feedback

  if (dGainO >= 8.5) {
    GO = 0x06;
  } else if (dGainO >= 5.5) {
    GO = 0x05;
  } else if (dGainO >= 4) {
    GO = 0x04;
  } else if (dGainO >= 3.1) {
    GO = 0x03;
  } else if (dGainO >= 2.5) {
    GO = 0x02;
  } else if (dGainO >= 2.1) {
    GO = 0x01;
  } else {
    GO = 0x00; // Gain of 2
  }

  // 0x00 - V_in1 = V_inp; V_in2 = V_inn;
  // 0x01 - V_in1 = V_inn; V_in1 = V_inp;
  uint8_t GI3 = 0x00;

  // Frontend gain table
  // 0x00 - 4
  // 0x01 - 8
  // 0x02 - 16
  // 0x03 - 23.27
  // 0x04 - 32
  // 0x05 - 42.67
  // 0x06 - 64
  // 0x07 - 128

  if (dGainF >= 128) {
    GI = 0x07;
  } else if (dGainF >= 63.5) {
    GI = 0x06;
  } else if (dGainF >= 42) {
    GI = 0x05;
  } else if (dGainF >= 31.5) {
    GI = 0x04;
  } else if (dGainF >= 23.5) {
    GI = 0x03;
  } else if (dGainF >= 15.5) {
    GI = 0x02;
  } else if (dGainF >= 7.5) {
    GI = 0x01;
  } else {
    GI = 0x00; // Gain is 4
  }

  uint8_t RFB = 0x00;

  // Assume 3.3 volt reference
  uint8_t OS = abs(dOffset) / (Vcc * 0.85);

  uint8_t OS5 = 0x00;

  if (dOffset < 0) {
    OS5 = 0x01;
  }

  // Limits OS register for magnitude, prevents overflow into negative offset and other registers
  if (OS > 0x0F) {
    OS = 0x0F;
  }


  // Starts with 1 bits
  result = OWD; result = result << 3;
  result |= GO; result = result << 1;
  result |= GI3; result = result << 3;
  result |= GI; result = result << 3;
  result |= RFB; result = result << 1;
  result |= OS5; result = result << 4;
  result |= OS;

  return result;
}

/**
*   @brief  Computes setting hex needed to represent intended configuration and over/under scale limits
*   @details Not utilized, but coded for potential future use
*
*   @return Resulting 16-bit hex value for setting and value
**/
uint16_t PGAConfigLimit() {
  uint16_t result;

  uint8_t RFB = 0x00;

  // Table 6-11 in User manual
  // Modulate chopping clock
  uint8_t CLK_CFG = 0x01;

  // Enable ext fault comp group
  uint8_t EXTEN = 0x00;

  // Enable int fault comp group
  uint8_t INTEN = 0x00;

  // V_out polarity (1-high, 0-low) when ext fault detected
  uint8_t EXTPOL = 0x00;

  // V_out polarity (1-high, 0-low) when int fault detected
  uint8_t INTPOL = 0x00;

  // Over/under-scale Limit Enable
  uint8_t OUEN = 0x00;

  // 0x00 - 0.9708 V_ref
  // 0x01 - 0.9610 V_ref
  // 0x02 - 0.9394 V_ref
  // 0x03 - 0.9160 V_ref
  // 0x04 - 0.9102 V_ref
  // 0x05 - 0.7324 V_ref
  // 0x06 - 0.5528 V_ref
  // 0x07 - Reserved
  uint8_t HL = 0x00;

  // 0x00 - 0.02540 V_ref
  // 0x01 - 0.02930 V_ref
  // 0x02 - 0.03516 V_ref
  // 0x03 - 0.03906 V_ref
  // 0x04 - 0.04492 V_ref
  // 0x05 - 0.05078 V_ref
  // 0x06 - 0.05468 V_ref
  // 0x07 - 0.06054 V_ref
  uint8_t LL = 0x00;

  // Starts with 2 bits
  result = RFB; result = result << 2;
  result |= CLK_CFG; result = result << 1;
  result |= EXTEN; result = result << 1;
  result |= INTEN; result = result << 1;
  result |= EXTPOL; result = result << 1;
  result |= INTPOL; result = result << 1;
  result |= RFB; result = result << 1;
  result |= OUEN; result = result << 3;
  result |= HL; result = result << 3;
  result |= LL;

  return result;
}

/**
*   @brief  Computes setting hex needed to represent intended temperature control
*   @details Not utilized, but coded for potential future use
*
*   @return Resulting 16-bit hex value for setting and value
**/
uint16_t TempADCCtrl() {
  uint16_t result;

  uint8_t RFB = 0x00;

  // Enable twice conversion speed for temp ADC
  uint8_t ADC2X = 0x00;

  // Start or restart the ADC
  uint8_t ADCS = 0x01;

  // Enable 7microA TEMP_in current source (I_temp)
  uint8_t ISEN = 0x00;

  // Enable continuous conversion
  uint8_t CEN = 0x00;

  // Enable Internal temp mode
  // Set ADC2X to 0; AREN to 0; RV, M, G to 00;
  uint8_t TEN = 0x00;

  // Enable internal 2.048 V reference, ignores RV
  uint8_t AREN = 0x00;

  // External temp ref
  // 0x00 - V_ref
  // 0x01 - V_exc
  // 0x02 - V_SA
  // 0x03 - Reserved
  uint8_t RV = 0x00;

  // Temp ADC Mux Select
  // 0x00 - TEMP_in (+), GND_a (-)
  // 0x01 - V_exc (+), TEMP_in (-)
  // 0x02 - V_out (+), GND_a (-)
  // 0x03 - V_ref (+), TEMP_in (-)
  uint8_t MUX = 0x00;

  // Temp ADC PGA Gain
  // 0x00 - 1
  // 0x01 - 2
  // 0x02 - 4
  // 0x03 - 8
  uint8_t GAIN = 0x00;

  // Refer to Table 6-17 in PGA309 User Manual (it's too long to comment O_o)
  uint8_t RES = 0x00;

  // Starts with 2 bits
  result = RFB; result = result << 1;
  result |= ADC2X; result = result << 1;
  result |= ADCS; result = result << 1;
  result |= ISEN; result = result << 1;
  result |= CEN; result = result << 1;
  result |= TEN; result = result << 1;
  result |= AREN; result = result << 2;
  result |= RV; result = result << 2;
  result |= MUX; result = result << 2;
  result |= GAIN; result = result << 2;
  result |= RES;

  return result;
}

/**
*   @brief  Computes setting hex needed to represent intended output enable counter control
*   @details Not utilized, but coded for potential future use
*
*   @return Resulting 16-bit hex value for setting and value
**/
uint16_t OutEnableCounterCtrl() {
  uint16_t result;

  uint8_t RFB = 0x00;

  // Temp ADC Delay (OEN decimal value * 14) ms
  int tempADCDelay = 98; // ms
  uint8_t DLY = tempADCDelay / 14;

  // V_out enable timeout
  int desiredTimeout = 2240; // ms
  uint8_t OEN = desiredTimeout / 14;

  // Starts with 4 bits
  result = RFB; result = result << 4;
  result |= DLY; result = result << 8;
  result |= OEN;

  return result;
}


/**
*   @brief   Converts ADC temperature readings into usable float data
*   @details Not utilized, but coded for potential future use
*   @param byte# First or second byte from 16-bit hex value
*   @return Resulting temperature from hex
**/
float tempConvert(uint8_t byte1, uint8_t byte2) {
  // Concat the two bytes
  uint16_t count = concatBytes(byte1, byte2);
  // Multiply the 2-byte by the multiplier to yield Celsius reading
  float tempC = 0.0625 * count;
  return tempC;
}