#ifndef PGA309_H
#define PGA309_H

void setupPGA();

void setSetting(byte setting, byte chan, float value);

float getSetting(byte setting, byte chan);

void viewSettings();

void accessRegister(uint8_t addr, bool isRead);

uint16_t writeToPGA(uint8_t addr, float value1, float value2, float value3);

uint16_t writeSelect(uint8_t addr, float value1, float value2, float value3);

void readFromPGA(uint8_t addr);

uint16_t fineOffsetAdjust(float frac);

uint16_t fineGainAdjust(float targetG);

uint16_t refCtrlLinear();

uint16_t PGAOffsetGain(float dGainF, float dGainO, float dOffset);

uint16_t PGAConfigLimit();

uint16_t TempADCCtrl();

uint16_t OutEnableCounterCtrl();

float tempConvert(uint8_t byte1, uint8_t byte2);

#endif
