/** 
 *  @file    tca9548a.cpp
 *  @author  Jacob Carducci
 *  @date    06/15/2017
 *  @version 0.2
 *  
 *  @brief Multiplexer function definitions.
 *
 *  @section DESCRIPTION
 *  
 * All things dealing with the TCA9548A multiplexers, including helpers.
 *
 **/

#include <stdint.h>
#include <i2c_t3.h>
#include "Arduino.h"
#include "pga309.h"
#include "hidmisc.h"
#include "tca9548a.h"
#include "constants.h"

uint8_t currPlex;
uint8_t targetPlex;
/**
*   @brief  Sets device and channel for multiplexer access through I2C to the first multiplexer and channel
*
*   @return void
**/
void plexReset(){
  targetPlex = plexA;
  currPlex = 0x01;
}

/**
*   @brief  Gets the plex device the I2C master is on
*
*   @return Current plex device address
**/
uint8_t getPlexDevice(){
    return targetPlex;
}

/**
*   @brief  Gets plex channel that the I2C master is on through the selected multiplexer
*
*   @return Current plex channel address
**/
uint8_t getPlexChan(){
    return currPlex;
}

/**
*   @brief  Manually selects a single I2C channel across all multiplexers
*
*   @param chan Specified I2C channel for corresponding amplifier
*   @return void
**/
void plexSelect(byte chan) {
  plexClear();
  int pgaChan = chan % 8;
  int plexChan = chan / 8;
  currPlex = 0x01 << pgaChan;
  targetPlex = plexA + plexChan;
  SendByteOverHID(PLEX_MANUAL_CHANGE,PLEX_PRINT_KEY);
  switchPlex(currPlex);
  SendByteOverHID(PLEX_MANUAL_FINISH,PLEX_PRINT_KEY);
}

/**
*   @brief  Clears all I2C channels across all multiplexers
*
*   @return void
**/
void plexClear() {
  SendByteOverHID(PLEX_MANUAL_CLEAR,PLEX_PRINT_KEY);
  targetPlex = plexC;
  switchPlex(0x00);
  targetPlex = plexB;
  switchPlex(0x00);
  targetPlex = plexA;
  switchPlex(0x00);
}

/**
*   @brief  Sends channel enable message to multiplexer with target address
*
*   @param ctrlReg Hex message for target multiplexer
*   @return void
**/
void switchPlex(uint8_t ctrlReg) {   
  uint8_t regState = 0xFF;
  SendByteOverHID(PLEX_SEND_DEVICE,targetPlex,PLEX_PRINT_ALL);
  SendByteOverHID(PLEX_REG_DESIRED,ctrlReg,PLEX_PRINT_ALL);
  Wire2.beginTransmission(targetPlex);
  Wire2.write(ctrlReg);
  Wire2.endTransmission();
  
  SendByteOverHID(PLEX_SEND_VERIFY,PLEX_PRINT_ALL);
  Wire2.requestFrom(targetPlex, 1, I2C_NOSTOP);
  while (Wire2.available()) {
    regState = Wire2.readByte();
  }
  
  if (regState == ctrlReg) {
    SendByteOverHID(PLEX_REG_NEW,regState,PLEX_PRINT_ALL);
  }
  // Check if communication failed
  printStatus(true,MINIMAL_PRINT);
  printErrors(true,MINIMAL_PRINT);
  printStatus(false,MINIMAL_PRINT);
  printErrors(false,MINIMAL_PRINT);
  SendByteOverHID(MISC_DIVIDER, PLEX_PRINT_ALL);
}

/**
*   @brief  Test routine for accessing all multiplexers in a row and sending back status  
*
*   @return void
**/
void plexRoutine() {
  int numChan = channels + 3;
  for (byte i = 0; i < numChan; i++) {
    plexSelect(i);
    accessRegister(0x01, true);
    accessRegister(0x02, true);
    accessRegister(0x04, true);
    SendByteOverHID(MISC_DIVIDER,PLEX_PRINT_KEY);
  }
}
