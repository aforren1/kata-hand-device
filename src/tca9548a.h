#ifndef TCA9548A_H
#define TCA9548A_H

typedef unsigned char byte;

void plexReset();

uint8_t getPlexDevice();

uint8_t getPlexChan();

// manual multiplexer select
// manually switch multiplex switch
void plexSelect(byte chan);

void plexClear();

void switchPlex(uint8_t ctrlReg);

void plexRoutine();

#endif
